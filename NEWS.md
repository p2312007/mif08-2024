<!-- LTeX: language=fr -->
# Nouvelles du cours

Les nouvelles du cours apparaîtront ici au fur et à mesure.

## 14/03/224 : précisions sur associativité et priorité en ANTLR

J'ai ajouté quelques précisions sur la gestion de l'associativité et des
priorités des opérateurs avec ANTLR dans les transparents de cours « lexing,
parsing » :

https://compil-lyon.gitlabpages.inria.fr/mif08-files-2024/capmif_cours02_lexing_parsing.pdf

Slides 15/36 (Lexer rules : dealing with ambiguities) et 32/36 (Parser rules :
dealing with ambiguities).

## 5/03/2024 : TP noté le 21 mars

En début de séance de TP le 21 mars, nous aurons une courte épreuve de TP (20
minutes). Vous avez un sujet « pour s'entraîner » sur la page du cours,
[CC2/cc2-sujet-exemple.pdf](CC2/cc2-sujet-exemple.pdf). Le sujet le jour J sera
très similaire, si vous avez fait ce sujet d'entraînement le TP noté devrait
être assez facile.

## 19/02/2024 : QCM noté ce jeudi

Comme annoncé en CM, vous aurez un examen court, de type QCM, noté, en début de
TD à 11h30 ce jeudi (durée environ 15 minutes). Les consignes détaillées sont
ici :
[QCM.md](https://forge.univ-lyon1.fr/matthieu.moy/mif08-2024/-/blob/main/QCM.md?ref_type=heads).
N'oubliez pas d'apporter votre documentation RiscV, et arrivez à l'heure.

## 15/02/2024 : VM réparées, QCM, fin du TP

J'avais fait plusieurs bêtises à propos des VM (à utiliser en dernier recours si
vous ne pouvez utiliser ni le .tar.gz ni Docker), normalement tout est réparé.
Les IP des VM ont changées (cf. [VM.md](VM.md)).

Je rappelle que vous avez un QCM à faire sur TOMUSS (deadlines indiquées sur la
page d'accueil, [README.md](README.md)), la note ne compte pas dans la moyenne,
mais je prévois un petit contrôle en TD la semaine prochaine qui sera très
inspiré du QCM (et qui lui donnera une note comptée dans la moyenne) ...

Très peu d'entre vous ont fait l'ensemble du TP ce matin. C'est normal, le sujet
est un peu long. Assurez-vous que vous êtes à l'aise avec Python. Vous n'avez
pas forcément besoin de faire toutes les questions Python du TP, mais vérifiez
que vous êtes à l'aise avec les notions, y compris le typage statique avec
Pyright en fin de TP. C'est aussi indispensable que vous soyez capables
d'assembler et exécuter des programmes RiscV (sur vos machines ou celles de la
fac), sinon vous perdrez beaucoup de temps à installer tout ça quand on arrivera
à la génération de code plus tard dans le semestre.
