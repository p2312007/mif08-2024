# Generated from Arit.g4 by ANTLR 4.13.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,10,48,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,1,0,4,0,10,8,0,11,0,12,
        0,11,1,0,1,0,1,1,1,1,1,1,1,1,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
        1,2,1,2,1,2,1,2,1,2,5,2,34,8,2,10,2,12,2,37,9,2,1,3,1,3,1,3,1,3,
        1,3,1,3,1,3,3,3,46,8,3,1,3,0,1,4,4,0,2,4,6,0,0,47,0,9,1,0,0,0,2,
        15,1,0,0,0,4,19,1,0,0,0,6,45,1,0,0,0,8,10,3,2,1,0,9,8,1,0,0,0,10,
        11,1,0,0,0,11,9,1,0,0,0,11,12,1,0,0,0,12,13,1,0,0,0,13,14,5,0,0,
        1,14,1,1,0,0,0,15,16,3,4,2,0,16,17,5,3,0,0,17,18,6,1,-1,0,18,3,1,
        0,0,0,19,20,6,2,-1,0,20,21,3,6,3,0,21,22,6,2,-1,0,22,35,1,0,0,0,
        23,24,10,3,0,0,24,25,5,6,0,0,25,26,3,4,2,4,26,27,6,2,-1,0,27,34,
        1,0,0,0,28,29,10,2,0,0,29,30,5,4,0,0,30,31,3,4,2,3,31,32,6,2,-1,
        0,32,34,1,0,0,0,33,23,1,0,0,0,33,28,1,0,0,0,34,37,1,0,0,0,35,33,
        1,0,0,0,35,36,1,0,0,0,36,5,1,0,0,0,37,35,1,0,0,0,38,39,5,7,0,0,39,
        46,6,3,-1,0,40,41,5,1,0,0,41,42,3,4,2,0,42,43,5,2,0,0,43,44,6,3,
        -1,0,44,46,1,0,0,0,45,38,1,0,0,0,45,40,1,0,0,0,46,7,1,0,0,0,4,11,
        33,35,45
    ]

class AritParser ( Parser ):

    grammarFileName = "Arit.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'('", "')'", "';'", "'+'", "'-'", "'*'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "SCOL", "PLUS", 
                      "MINUS", "MULT", "INT", "COMMENT", "NEWLINE", "WS" ]

    RULE_prog = 0
    RULE_statement = 1
    RULE_expr = 2
    RULE_atom = 3

    ruleNames =  [ "prog", "statement", "expr", "atom" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    SCOL=3
    PLUS=4
    MINUS=5
    MULT=6
    INT=7
    COMMENT=8
    NEWLINE=9
    WS=10

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(AritParser.EOF, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritParser.StatementContext)
            else:
                return self.getTypedRuleContext(AritParser.StatementContext,i)


        def getRuleIndex(self):
            return AritParser.RULE_prog

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProg" ):
                listener.enterProg(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProg" ):
                listener.exitProg(self)




    def prog(self):

        localctx = AritParser.ProgContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_prog)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 9 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 8
                self.statement()
                self.state = 11 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==1 or _la==7):
                    break

            self.state = 13
            self.match(AritParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self._expr = None # ExprContext

        def expr(self):
            return self.getTypedRuleContext(AritParser.ExprContext,0)


        def SCOL(self):
            return self.getToken(AritParser.SCOL, 0)

        def getRuleIndex(self):
            return AritParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)




    def statement(self):

        localctx = AritParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 15
            localctx._expr = self.expr(0)
            self.state = 16
            self.match(AritParser.SCOL)
            print((None if localctx._expr is None else self._input.getText(localctx._expr.start,localctx._expr.stop))+" = "+str(localctx._expr.val))
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.val = None
            self.e1 = None # ExprContext
            self.a = None # AtomContext
            self.e2 = None # ExprContext

        def atom(self):
            return self.getTypedRuleContext(AritParser.AtomContext,0)


        def MULT(self):
            return self.getToken(AritParser.MULT, 0)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(AritParser.ExprContext)
            else:
                return self.getTypedRuleContext(AritParser.ExprContext,i)


        def PLUS(self):
            return self.getToken(AritParser.PLUS, 0)

        def getRuleIndex(self):
            return AritParser.RULE_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpr" ):
                listener.enterExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpr" ):
                listener.exitExpr(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = AritParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 4
        self.enterRecursionRule(localctx, 4, self.RULE_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 20
            localctx.a = self.atom()
            localctx.val = localctx.a.val
            self._ctx.stop = self._input.LT(-1)
            self.state = 35
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 33
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
                    if la_ == 1:
                        localctx = AritParser.ExprContext(self, _parentctx, _parentState)
                        localctx.e1 = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 23
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 24
                        self.match(AritParser.MULT)
                        self.state = 25
                        localctx.e2 = self.expr(4)
                        localctx.val = 42
                        pass

                    elif la_ == 2:
                        localctx = AritParser.ExprContext(self, _parentctx, _parentState)
                        localctx.e1 = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 28
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 29
                        self.match(AritParser.PLUS)
                        self.state = 30
                        localctx.e2 = self.expr(3)
                        localctx.val = localctx.e1.val + localctx.e2.val
                        pass

             
                self.state = 37
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class AtomContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.val = None
            self._INT = None # Token
            self._expr = None # ExprContext

        def INT(self):
            return self.getToken(AritParser.INT, 0)

        def expr(self):
            return self.getTypedRuleContext(AritParser.ExprContext,0)


        def getRuleIndex(self):
            return AritParser.RULE_atom

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAtom" ):
                listener.enterAtom(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAtom" ):
                listener.exitAtom(self)




    def atom(self):

        localctx = AritParser.AtomContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_atom)
        try:
            self.state = 45
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [7]:
                self.enterOuterAlt(localctx, 1)
                self.state = 38
                localctx._INT = self.match(AritParser.INT)
                localctx.val = int((None if localctx._INT is None else localctx._INT.text))
                pass
            elif token in [1]:
                self.enterOuterAlt(localctx, 2)
                self.state = 40
                self.match(AritParser.T__0)
                self.state = 41
                localctx._expr = self.expr(0)
                self.state = 42
                self.match(AritParser.T__1)
                localctx.val=localctx._expr.val
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[2] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         




